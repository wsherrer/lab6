/*******************
William Sherrer
wsherre
Lab 5
Lab Section: 004
Anurata Hridi
*******************/
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
    Suit suit;
    int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
    // IMPLEMENT as instructed below
    /*This is to seed the random generator */
    srand(unsigned (time(0)));

    //declares struct array and variables
    Card deck[52];
    int i;
    int num = 0;
    int val = 0;

    /*creates the deck of cards by assigning number values and suit
    to the struct array*/
    while(num < 4){
        for(i = 2; i <= 14; ++i){
            deck[val].value = i;
            deck[val].suit = Suit(num);
            val++;
        }
        num += 1;
    }

    //randomly shuffles the deck
    random_shuffle(deck, deck + 52 ,myrandom);
    /* sorts the first five cards of the deck by utilizing
     bool function*/
    sort(deck, deck + 5, suit_order);

    //outputs the sorted cards to the terminal
    for(i = 0; i < 5; ++i){
        cout << setw(10) << get_card_name(deck[i]);
        cout << " of " << get_suit_code(deck[i]) << endl;
    }



    /*Create a deck of cards of size 52 (hint this should be an array) and
     *initialize the deck*/


    /*After the deck is created and initialzed we call random_shuffle() see the
     *notes to determine the parameters to pass in.*/


    /*Build a hand of 5 cards from the first five cards of the deck created
     *above*/


    /*Sort the cards.  Links to how to call this function is in the specs
     *provided*/


    /*Now print the hand below. You will use the functions get_card_name and
     *get_suit_code */
    return 0;
}

/*This function will be passed to the sort funtion. Hints on how to implement
 * this is in the specifications document.*/
bool suit_order(const Card& lhs, const Card& rhs) {

    //returns true if the suit of the left card is less than the right
    if(lhs.suit < rhs.suit)
        return true;

    else if(lhs.suit > rhs.suit)
        return false;

    else{
        /*if the suit values are equal then it compares the card values
        and returns true if the left card is less than the right*/
        if(lhs.value < rhs.value)
            return true;
        else
            return false;
    }
}

//outptus the little picture of the suit
string get_suit_code(Card& c) {
    switch (c.suit) {
        case SPADES:    return "\u2660";
        case HEARTS:    return "\u2661";
        case DIAMONDS:  return "\u2662";
        case CLUBS:     return "\u2663";
        default:        return "";
    }
}

//outputs the name of the card
string get_card_name(Card& c) {
    switch (c.value){
        case 11:        return "Jack";
        case 12:        return "Queen";
        case 13:        return "King";
        case 14:        return "Ace";
        default:        return to_string(c.value);
    }
}
